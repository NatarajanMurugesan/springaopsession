package com.aspire.springaop;

public interface Hello {
	public void sayHello() throws Exception;

	public void echo();

	public void foo();

}
