package com.aspire.springaop;

import org.springframework.context.*;
import org.springframework.context.support.*;

public class Client {
	public static void main(String[] str) throws Exception {
		ApplicationContext ctx = new ClassPathXmlApplicationContext(
				"springconfig.xml");
		Hello h = (Hello) ctx.getBean("HT");
		ctx = null;
		try {
			h.sayHello();
		} catch (Exception err) {
			System.out.println(err);
		}
		h.echo();
		h.foo();

	}
}