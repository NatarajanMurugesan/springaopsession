package com.aspire.springaop;

public class HelloWorld implements Hello {
	public void sayHello() throws Exception {

		System.out.println("      HELLO WORLD.....");
		throw new Exception("Test Message");
	}

	public void echo() {

		System.out.println("Echo.....");
	}

	public void foo() {

		System.out.println("foo.....");
	}
}