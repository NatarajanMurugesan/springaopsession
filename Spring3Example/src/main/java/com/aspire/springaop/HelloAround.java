package com.aspire.springaop;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class HelloAround implements MethodInterceptor
{
	public Object invoke(MethodInvocation m)throws Throwable
	{
		Object result=null;
		System.out.println("\tAROUND.....");
		result=m.proceed();
		Shape s1=new Shape();
		s1.draw();
		System.out.println("\tAFTER PROCEED.....");
		return result;
	}
}
