package com.aspire.springaop;

import org.springframework.aop.ThrowsAdvice;

public class HelloThrowsAdvice implements ThrowsAdvice {
	public void afterThrowing(Exception e) {
		System.out.println("    ERROR in afterThrowing: " + e.getMessage());
	}
}
