package com.aspire.springaop;

import java.lang.reflect.Method;
import org.springframework.aop.MethodBeforeAdvice;

public class HelloBeforeAdvice implements MethodBeforeAdvice {
	public void before(Method m, Object[] source, Object target) {
		System.out.println("\t     BEFORE.....");
		System.out.print("   MName: " + m.getName());
		System.out.print("   target: " + target);
		System.out.println("    ");
	}
}
