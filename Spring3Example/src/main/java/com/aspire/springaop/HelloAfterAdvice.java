package com.aspire.springaop;

import java.lang.reflect.Method;
import org.springframework.aop.AfterReturningAdvice;

/**
 * @author natarajan.murugesan
 *
 */
public class HelloAfterAdvice implements AfterReturningAdvice {
	public void afterReturning(Object o, Method m, Object[] source,
			Object target) {
		System.out.println("\t     AFTER RETURNING.....");
	}
}
