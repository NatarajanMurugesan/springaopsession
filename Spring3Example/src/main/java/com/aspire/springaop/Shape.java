package com.aspire.springaop;

public class Shape {
	public Shape() {
		System.out.println("Shape " + hashCode());
	}

	public void draw() {
		System.out.println("Shape.draw ");
	}

}