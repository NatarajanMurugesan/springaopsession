package com.aspire.proxy;

/**
 * @author natarajan.murugesan
 *
 */
public class RealImage implements Image {

	private String fileName = null;

	public RealImage(String fileName) {
		super();
		this.fileName = fileName;
		loadImageFromDisk();
	}

	private void loadImageFromDisk() {
		System.out.println("Loading   " + fileName);
	}

	@Override
	public void displayImage() {
		System.out.println("Displaying " + fileName);

	}

}
