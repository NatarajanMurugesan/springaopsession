package com.aspire.proxy;

public interface Image {
	public void displayImage();
}
