package com.aspire.proxy;

/**
 * A proxy may hide information about the real object to the client.<br/>
 * A proxy may perform optimization like on demand loading.<br/>
 * A proxy may do additional house-keeping job like audit tasks.<br/>
 * Proxy design pattern is also known as surrogate design pattern.<br/>
 * 
 * @author natarajan.murugesan
 *
 */
public class ProxyImage implements Image {

	private RealImage image = null;
	private String fileName = null;

	public ProxyImage(final String fileName) {
		this.fileName = fileName;
	}

	/* 
	 * 
	 */
	@Override
	public void displayImage() {
		if (image == null) {
			image = new RealImage(fileName);
		}
		image.displayImage();
	}

}
