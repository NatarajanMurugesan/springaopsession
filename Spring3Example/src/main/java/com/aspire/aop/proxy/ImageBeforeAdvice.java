package com.aspire.aop.proxy;

import java.lang.reflect.Method;

import org.springframework.aop.MethodBeforeAdvice;

import com.aspire.proxy.Image;
import com.aspire.proxy.RealImage;

public class ImageBeforeAdvice implements MethodBeforeAdvice {

	@Override
	public void before(Method arg0, Object[] source, Object target)
			throws Throwable {
		Image image =(RealImage)target;
		if(image==null){
		// image = new RealImage();	
		}
		
	}

}
